create role web_anon nologin;
grant usage on schema public to web_anon;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO web_anon;
