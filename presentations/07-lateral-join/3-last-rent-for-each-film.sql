with all_rentals (film, rental_date, iteration) as (
    select f.title, rental_date, row_number() over (partition by f.title order by rental_date desc)
    from film f
    join inventory i on f.film_id = i.film_id
    join rental r on i.inventory_id = r.inventory_id
    order by title, rental_date desc
 )
select film, rental_date
from all_rentals
where iteration <= 3;

select film.title, recently_rented.rental_date
from film
left join lateral (
    select *
    from inventory i
    join rental r on i.inventory_id = r.inventory_id
    where i.film_id = film.film_id -- film from outer query
    order by rental_date desc
    limit 3
) as recently_rented on film.film_id = recently_rented.film_id
order by film.title;

-- 1. for each line of the first table (film)
-- 2. we run the inner query, independently
-- 3. the ON condition can be replaced by `TRUE` because we already filtered by line
-- 4. (parenthesis) make clear the query is run multiple time