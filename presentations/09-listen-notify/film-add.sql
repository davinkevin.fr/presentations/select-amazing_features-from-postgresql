create table film_with_notification (
    film_id integer default nextval('film_film_id_seq'::regclass) NOT NULL,
    title character varying(255) not null
);

create or replace function notify_new_film()
 returns trigger
 language plpgsql
as $$
declare
  channel text := TG_ARGV[0];
begin
  perform (
     with payload as (select NEW.film_id as id, NEW.title as title)
     select pg_notify(channel, row_to_json(payload)::text) from payload
  );
  return null;
end;
$$;

create trigger notify_clients
after insert on film_with_notification
for each row execute procedure notify_new_film('film.add');

listen "film.add";

insert into film_with_notification(title)
values
    ('Speed'), ('John Wick'), ('Point Break'),
    ('The Devil''s Advocate'), ('The Lake House');