select a.last_name, a.first_name, a.actor_id, count(*) as "# of film"
from actor a
inner join film_actor fa on a.actor_id = fa.actor_id
inner join film f on fa.film_id = f.film_id
group by a.last_name, a.first_name, a.actor_id
order by 4 desc;

-- 'DEGENERES', 'GINA', (id=107) with 42 movies
-- I want to apply a discount of 10% on them…

select film_id, title, rental_rate
from film
where film_id in (select film_id from film_actor where actor_id = 107)
order by rental_rate desc, title;

select film_id, title, rental_rate
from film
where film_id = 409;


-- with inner query, standard update
update film
set rental_rate = rental_rate * 1.1
from actor a
inner join film_actor fa on a.actor_id = fa.actor_id
where a.first_name = 'GINA'
and a.last_name = 'DEGENERES'
and film.film_id = fa.film_id;

-- with 01-CTE
with film_from_gina_degeneres (film_id) as (
    select f.film_id
    from actor a
    inner join film_actor fa on a.actor_id = fa.actor_id
    inner join film f on fa.film_id = f.film_id
    where a.first_name = 'GINA'
    and a.last_name = 'DEGENERES'
    and f.film_id = fa.film_id
)
update film
set rental_rate = rental_rate * 1.1
where film.film_id in (select * from film_from_gina_degeneres);