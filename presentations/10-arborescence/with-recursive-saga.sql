drop table if exists film_with_previous;
create table film_with_previous (
    film_id serial primary key,
    title character varying(255) not null,

    previous integer references film_with_previous(film_id)
);

delete from film_with_previous where film_id >= 1;
insert into film_with_previous (film_id, title, previous)
values
    (1, 'Terminator', null),
    (2, 'Terminator 2: Judgment Day', 1),
    (3, 'Terminator 3: Rise of the Machines', 2),
    (4, 'Terminator Salvation', 3),
    (5, 'Terminator Genisys', 1),
    (6, 'Terminator: Dark Fate', 2);

with recursive saga as (
        select f.film_id, f.title, f.previous
        from film_with_previous f
        where f.film_id = 6
    union all
        select f.film_id, f.title, f.previous
        from film_with_previous f
        inner join saga s on s.previous = f.film_id
)
select film_id, title, previous
from saga
order by film_id;