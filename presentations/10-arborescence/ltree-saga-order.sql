create extension ltree;

create table film_with_saga_order (
    film_id integer default nextval('film_film_id_seq'::regclass) NOT NULL,
    title character varying(255) not null,
    saga_order ltree
);

delete from film_with_saga_order where film_id >= 1;
insert into film_with_saga_order (film_id, title, saga_order)
values
    (1, 'Terminator', '1'),
    (2, 'Terminator 2: Judgment Day', '1.2'),
    (3, 'Terminator 3: Rise of the Machines', '1.2.3'),
    (4, 'Terminator Salvation', '1.2.3.4'),
    (5, 'Terminator Genisys', '1.5'),
    (6, 'Terminator: Dark Fate', '1.2.6');

select *
from film_with_saga_order;

-- with film_parents as (
--     select unnest(string_to_array(saga_order::text, '.'))
--     from film_with_saga_order
--     where film_id = 6
-- )
-- select *
-- from film_with_saga_order
-- where film_id::text in (select * from film_parents);

with selected_movie as (
    select saga_order
    from film_with_saga_order
    where film_id = 6
)
select *
from film_with_saga_order
where saga_order @> (select saga_order from selected_movie);
-- @> means '[ltree] is ancestor of [ltree]

