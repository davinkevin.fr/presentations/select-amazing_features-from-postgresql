create extension file_fdw;
create server file_server foreign data wrapper file_fdw;

create foreign table passwd (
    username text,
    pass text,
    uid int4,
    gid int4,
    gecos text,
    home text,
    shell text
) server file_server
options (format 'text', filename '/etc/passwd', delimiter ':', null '');

select username, uid, gid, home, shell
from passwd
where shell ~* '.*bash.*';

drop foreign table if exists fs;
create foreign table fs (
    inode text,
    block text,
    permission text,
    hard_link text,
    owner text,
    "group" text,
    size int,
    last_modification text,
    pathname text
) server file_server
options (format 'text', program 'find / -not \( -path /dev -prune \) -not \( -path /proc -prune \) -not \( -path /sys -prune \) -ls | awk ''{printf( "%s|%s|%s|%s|%s|%s|%s|%s %s %s|%s\n", $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11 )}''', delimiter '|', null '');

select pathname, owner, "group", size
from fs
where permission ~ '.......rwx'
order by size desc
limit 5;