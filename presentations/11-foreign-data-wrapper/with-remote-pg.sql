create extension postgres_fdw;

create server sakila
foreign data wrapper postgres_fdw
options (host 'amazing-pg', port '5432', dbname 'sakila');

create user mapping for postgres
server sakila
options (user 'postgres', password 'gjITg2bOO33DlbYju27wK2WoOlI2');

create foreign table remote_film (
    film_id integer not null,
    title character varying(255) not null
)
server sakila
options (schema_name 'public', table_name 'film');

select *
from remote_film;

select film_id, title
from film
order by film_id
limit 5;

insert into remote_film (film_id, title)
values (7777, 'foo');