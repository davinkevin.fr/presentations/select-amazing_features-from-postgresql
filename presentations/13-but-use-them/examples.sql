create table xyz (
    json_data jsonb default '{}'
)

-- Use PostGIS for geo-queries
create extension postgis;

create extension "uuid-ossp";
DROP table xyz;
create table xyz (
    id uuid not null default uuid_generate_v4(),
    title text not null,
    fts_title tsvector generated always as (to_tsvector('english', title)) STORED
)