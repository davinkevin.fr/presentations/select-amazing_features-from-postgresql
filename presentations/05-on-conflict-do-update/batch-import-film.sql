create table film_for_batch_import (
    film_id integer default nextval('film_film_id_seq'::regclass) not null,
    title character varying(255) not null,
    last_update timestamp with time zone default now() not null,

    constraint unique_title unique (title)
);

insert into film_for_batch_import (title)
select title
from film;

-- 1. Add a constraint in the DB, please use them!
-- 2. We check the current db state for some films

select title, last_update
from film_for_batch_import
where title in ('ACADEMY DINOSAUR', 'ACE GOLDFINGER', 'JUST FOR DEMO');

-- 3. We do an insert from a batch or an uncleaned source…


-- truncate film_for_batch_import;
insert into film_for_batch_import (title, last_update)
values ('ACADEMY DINOSAUR', '2023-01-02 03:04:05.678910+00'::timestamptz),
       ('ACE GOLDFINGER', '2023-01-02 03:04:05.678910+00'::timestamptz),
       ('JUST FOR DEMO', '2023-01-02 03:04:05.678910+00'::timestamptz)
on conflict on constraint unique_title do update
set last_update = now();

-- 4. we can check only one field has been updated for already existing movies
-- 5. but one has been created in the same time…

select title, last_update
from film_for_batch_import
where title in ('ACADEMY DINOSAUR', 'ACE GOLDFINGER', 'JUST FOR DEMO');

-- 6. only one `on conflict` -> `do update` possible per query… 😭

select '2023-01-02 03:04:05.678910+00'::timestamptz