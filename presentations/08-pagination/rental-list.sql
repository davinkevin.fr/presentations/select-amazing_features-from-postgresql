
-- page 3
select
    f.title as film,
    c.first_name || ' ' || c.last_name as "rented by",
    r.rental_date::timestamp(0) as "from",
    r.rental_date,
    r.return_date::timestamp(0) as "to",
    r.return_date
from rental r
  inner join inventory i using(inventory_id)
  inner join film f using(film_id)
  inner join customer c using(customer_id)
order by r.rental_date, r.return_date desc
limit 5 offset 5*3;

-- page 4
select
    f.title as film,
    c.first_name || ' ' || c.last_name as "rented by",
    r.rental_date::timestamp(0) as "from",
    r.rental_date,
    r.return_date::timestamp(0) as "to",
    r.return_date
from rental r
  inner join inventory i using(inventory_id)
  inner join film f using(film_id)
  inner join customer c using(customer_id)
order by r.rental_date, r.return_date desc
limit 5 offset 5*4;

---

--- Last element of Page 3
--- r.rental_date = '2005-05-25 01:48:41.000000'
--- r.return_date = '2005-05-27 02:20:41.000000'
select
    f.title as film,
    c.first_name || ' ' || c.last_name as "rented by",
    r.rental_date::timestamp(0) as "from",
    r.return_date::timestamp(0) as "to"
from rental r
  inner join inventory i using(inventory_id)
  inner join film f using(film_id)
  inner join customer c using(customer_id)
where (r.rental_date, r.return_date) > ('2005-05-25 01:48:41.000000'::timestamp, '2005-05-27 02:20:41.000000'::timestamp)
-- where   r.rental_date > '2005-05-25 01:48:41.000000'::timestamp
-- and     r.last_update > '2005-05-27 02:20:41.000000'::timestamp
order by r.rental_date, r.return_date desc
limit 5;