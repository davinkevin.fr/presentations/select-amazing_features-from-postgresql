create table film_with_isan (
    film_id integer default nextval('film_film_id_seq'::regclass) NOT NULL,
    title character varying(255) not null,
    isan character varying(255),

    constraint has_valid_isan check (
        isan is null or
        isan ~* 'ISAN [0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-z]-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-z]'
        --      ISAN        0000-       0001-       8947-       0000-       8-       0000-       0000-       D
    ),
    constraint isan_is_unique unique (isan)
);

-- alter table film_with_isan add column isan text
--     constraint has_valid_isan check (
--             isan is null or
--             isan ~ 'ISAN [0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-z]-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-z]'
--         --      ISAN        0000-       0001-       8947-       0000-       8-       0000-       0000-       D
--         )
--     constraint isan_is_unique unique;

insert into film_with_isan (title, isan)
values ('NO_ISAN', null);

insert into film_with_isan (title, isan)
values ('INVALID_ISAN','63998367-c0c9-4fe0-82fc-ba4c96890ee1');

insert into film_with_isan (title, isan)
values ('REAL_ISAN', 'ISAN 0000-0001-68EC-0000-X-0000-0000-C');