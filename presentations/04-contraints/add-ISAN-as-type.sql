drop domain isan cascade ;
drop type isan_type cascade ;

create type isan_type as (
    block_1 varchar(4), block_2 varchar(4), block_3 varchar(4), block_4 varchar(4),
    block_5 varchar(1), block_6 varchar(4), block_7 varchar(4), block_8 varchar(1)
);

create or replace function is_valid_isan(isan_type)
    returns boolean
    immutable
    language sql
as $$
    select (
        ($1).block_1 is not null and ($1).block_1 ~* '[0-9a-f]{4}'
        and ($1).block_2 is not null and ($1).block_2 ~* '[0-9a-f]{4}'
        and ($1).block_3 is not null and ($1).block_3 ~* '[0-9a-f]{4}'
        and ($1).block_4 is not null and ($1).block_4 ~* '[0-9a-f]{4}'
        and ($1).block_5 is not null and ($1).block_5 ~* '[0-9a-f]'
        and ($1).block_6 is not null and ($1).block_6 ~* '[0-9a-f]{4}'
        and ($1).block_7 is not null and ($1).block_7 ~* '[0-9a-f]{4}'
        and ($1).block_8 is not null and ($1).block_8 ~* '[0-9a-f]'
    )
$$;


-- create domain isan as isan_type check (
--         (value).block_1 is not null and (value).block_1 ~* '[0-9a-f]{4}'
--     and (value).block_2 is not null and (value).block_2 ~* '[0-9a-f]{4}'
--     and (value).block_3 is not null and (value).block_3 ~* '[0-9a-f]{4}'
--     and (value).block_4 is not null and (value).block_4 ~* '[0-9a-f]{4}'
--     and (value).block_5 is not null and (value).block_5 ~* '[0-9a-f]'
--     and (value).block_6 is not null and (value).block_6 ~* '[0-9a-f]{4}'
--     and (value).block_7 is not null and (value).block_7 ~* '[0-9a-f]{4}'
--     and (value).block_8 is not null and (value).block_8 ~* '[0-9a-f]'
-- );
create domain isan as isan_type check (is_valid_isan(value));

select ('0000', '0001', '68ec', '0000', 'a', '0000', '0000', 'c')::isan as isan;
select (null, '0001', '68ec', '0000', 'a', '0000', '0000', 'c')::isan;

--- Utils

create or replace function isan_to_text(isan)
    returns text
    immutable
    language sql
as $$
    select
        format('ISAN %s-%s-%s-%s-%s-%s-%s-%s',
            upper($1.block_1),
            upper($1.block_2),
            upper($1.block_3),
            upper($1.block_4),
            upper($1.block_5),
            upper($1.block_6),
            upper($1.block_7),
            upper($1.block_8)
        )
$$;

select isan_to_text(('0000', '0001', '68ec', '0000', 'a', '0000', '0000', 'c')::isan);

create or replace function isan_exception(isan text)
    returns isan_type
    immutable
    language plpgsql
as $$
begin
    raise exception using errcode='22000', message=format('Invalid isan %L', isan);
end;
$$;

-- drop function text_to_isan(text);
create or replace function text_to_isan(text)
    returns isan
    immutable
    strict
    language sql
as $$
    with
        truncated_isan(isan) as ( select right($1, -5)),
        parsed(isan) as (
        select (
            split_part(isan, '-', 1),
            split_part(isan, '-', 2),
            split_part(isan, '-', 3),
            split_part(isan, '-', 4),
            split_part(isan, '-', 5),
            split_part(isan, '-', 6),
            split_part(isan, '-', 7),
            split_part(isan, '-', 8)
        )::isan_type
        from truncated_isan
    )
    select
        case is_valid_isan(parsed.isan) when true
            then parsed.isan::isan
            else isan_exception($1)
       end
    from parsed
$$;

select text_to_isan('ISAN 0000-0001-68EC-0000-A-0000-0000-C');
select text_to_isan('fjoqeijfoeqjioe');

-- operators

create or replace function isan_equals(a isan, b isan)
  returns boolean
  immutable
  language sql
as $$
    select
        upper(a.block_1) = upper(b.block_1)
    and upper(a.block_2) = upper(b.block_2)
    and upper(a.block_3) = upper(b.block_3)
    and upper(a.block_4) = upper(b.block_4)
    and upper(a.block_5) = upper(b.block_5)
    and upper(a.block_6) = upper(b.block_6)
    and upper(a.block_7) = upper(b.block_7)
    and upper(a.block_8) = upper(b.block_8)
$$;

create or replace function isan_not_equals(a isan, b isan)
  returns boolean
  immutable
    language sql
as $$
    select not(isan_equals(a, b))
$$;

select
    isan_equals(
      text_to_isan('ISAN 0000-0001-68EC-0000-A-0000-0000-C'),
      text_to_isan('ISAN 0000-0001-68EC-0000-A-0000-0000-C')
    ) as isan_equality,
    isan_not_equals(
        ('0000', '0001', '68ec', '0000', 'a', '0000', '0000', 'c')::isan,
        text_to_isan('ISAN 0000-0001-68EC-0000-A-0000-0000-C')
    ) as isan_different;

create operator = (
  leftarg = isan,
  rightarg = isan,
  procedure = isan_equals,
  commutator = =,
  negator = !=,
  hashes,
  merges
);

create operator != (
  leftarg = isan,
  rightarg = isan,
  function = isan_not_equals,
  commutator = !=,
  negator = =,
  hashes,
  merges
);

select text_to_isan('ISAN 0000-0001-68EC-0000-A-0000-0000-C') = text_to_isan('ISAN 0000-0001-68EC-0000-A-0000-0000-C') as isan_equality;
select ('0000', '0001', '68ec', '0000', 'a', '0000', '0000', 'c')::isan != text_to_isan('ISAN 0000-0001-68EC-0000-A-0000-0000-C') as isan_different;


--- final example
drop table if exists film_with_isan;
create table film_with_isan (
    film_id integer default nextval('film_film_id_seq'::regclass) NOT NULL,
    title character varying(255) not null,
    isan isan,
    constraint isan_is_unique unique (isan)
);

insert into film_with_isan (title, isan)
values
    ('REAL_ISAN', text_to_isan('ISAN 0000-0001-68EC-0000-A-0000-0000-C')),
    ('REAL_ISAN_2', text_to_isan('ISAN 1234-1234-68EC-1234-A-1234-1234-C'));

select *
from film_with_isan;

delete
from film_with_isan
where isan != text_to_isan('ISAN 0000-0001-68EC-0000-A-0000-0000-C');