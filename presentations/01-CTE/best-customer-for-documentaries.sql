select
    c.first_name || ' ' || c.last_name,
    sum(amount)
from
    customer c
        join (
            select r.customer_id
            from rental r
                 inner join inventory i on r.inventory_id = i.inventory_id
                 inner join film f on i.film_id = f.film_id
                 inner join film_category fc on f.film_id = fc.film_id
                 inner join category cat on cat.category_id = fc.category_id
            where cat.name = 'Documentary'
            group by r.customer_id
        ) r on c.customer_id = r.customer_id
        join payment ps on c.customer_id = ps.customer_id
group by c.first_name, c.last_name
order by 2 desc;

-- 1. Reading from right to left
-- 2. Impossible for human to simply define links between inner and outer query
-- 3. Lack of name for this sub query

with documentary_rentals as (
    select r.customer_id
    from rental r
             inner join inventory i on r.inventory_id = i.inventory_id
             inner join film f on i.film_id = f.film_id
             inner join film_category fc on f.film_id = fc.film_id
             inner join category cat on cat.category_id = fc.category_id
    where cat.name = 'Documentary'
    group by r.customer_id
)
select
    c.first_name || ' ' || c.last_name,
    sum(ps.amount)
from
    customer c
        join documentary_rentals r on c.customer_id = r.customer_id
        join payment ps on c.customer_id = ps.customer_id
group by c.first_name, c.last_name
order by 2 desc;