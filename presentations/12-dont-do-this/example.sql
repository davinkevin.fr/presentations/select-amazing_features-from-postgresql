create table xyz (
  date timestamp
);

create table xyz (
  aaa text
) inherits (film);

select *
from film
where title not in (select * from other_table);