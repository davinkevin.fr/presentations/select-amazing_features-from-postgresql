-- 1. Create a requested film table & index
create table film_for_merge (
    film_id integer default nextval('film_film_id_seq'::regclass) not null,
    title character varying(255) not null,
    rating mpaa_rating default 'G'::mpaa_rating,
    number_of_time_requested integer default 0
);

create table requested_film (
    film_id integer default nextval('film_film_id_seq'::regclass) not null,
    title character varying(255) not null,
    rating mpaa_rating default 'G'::mpaa_rating
);

insert into film_for_merge (title, rating)
select title, rating
from film;

-- 2. Add some film to it:
-- * 2 already exist
-- * 1 is new
-- * 1 is Rated-R, with a lot of violence
insert into requested_film (title, rating)
values
    ('ACADEMY DINOSAUR', 'PG'),
    ('ACE GOLDFINGER', 'G'),
    ('JUST FOR MERGE DEMO', 'NC-17'),
    ('TOO VIOLENT MOVIE', 'R');

select title, rating from requested_film order by title;

select title, number_of_time_requested from film_for_merge
where title in ('ACADEMY DINOSAUR', 'ACE GOLDFINGER', 'JUST FOR MERGE DEMO', 'TOO VIOLENT MOVIE')
order by title;

-- 3. we merge
merge into film_for_merge f
using requested_film rf
    on rf.title = f.title -- ISAN could be better 😅
when matched then
    update set number_of_time_requested = number_of_time_requested + 1
when not matched and rf.rating = 'R'::mpaa_rating then
    do nothing
when not matched then
    insert (title, rating)
    values (rf.title, rf.rating);

select title, number_of_time_requested
from film_for_merge
where title in (
    'ACADEMY DINOSAUR', 'ACE GOLDFINGER',
    'JUST FOR MERGE DEMO', 'TOO VIOLENT MOVIE'
)
order by title;