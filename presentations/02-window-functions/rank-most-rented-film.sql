
-- Evaluate what is the most rented movie from the database
-- 1. Create a 01-CTE to get this information

with film_order_by_rent_success(title, times_rented) as (
  select f.title, count(*) as "# of time rented"
  from film f
    inner join inventory i on f.film_id = i.film_id
    inner join rental r on i.inventory_id = r.inventory_id
  group by f.film_id, f.title
--   order by 2 desc, f.title
)
select
  title, times_rented,
  rank() over (order by times_rented desc),
  dense_rank() over (order by times_rented desc)
from film_order_by_rent_success
order by 3, title;

with film_order_by_rent_success(title, times_rented) as (
  select f.title, count(*) as "# of time rented"
  from film f
    inner join inventory i on f.film_id = i.film_id
    inner join rental r on i.inventory_id = r.inventory_id
  group by f.film_id, f.title
  order by 2
), film_ranked (title, times_ranted, rank, drank) as (
  select
    title, times_rented,
    rank() over (order by times_rented desc),
    dense_rank() over (order by times_rented desc)
  from film_order_by_rent_success
)
select title, times_ranted, rank, drank
from film_ranked
-- limit 3 -- 2. Solution without tie management… just pure luck for some.
-- where rank < 3 -- 3. ex-aequo are counted and limit the list
-- where drank < 3 -- 4. ex-aequo doesn't count, we group by positions
;