
-- Number of time each film has been rented

select count(*) as "# of time rented", title
from film
  inner join inventory i using (film_id)
  inner join rental r using(inventory_id)
  inner join customer c using(customer_id)
group by film_id, title
order by "# of time rented" desc, film_id;

-- Using movie `BUCKET BROTHERHOOD`
-- Display for each line the previous and next one who rented the film
SELECT
  c.first_name || ' ' || c.last_name as "rented by"
FROM film
  inner join inventory i using (film_id)
  inner join rental r using(inventory_id)
  inner join customer c using(customer_id)
where title = 'BUCKET BROTHERHOOD'
and inventory_id = 465
order by rental_date;

-- Using movie `BUCKET BROTHERHOOD`
-- Display for each line the previous and next one who rented the film
SELECT
  c.first_name || ' ' || c.last_name as "rented by",
  date_trunc('day', r.rental_date),
  lag(c.first_name || ' ' || c.last_name) over (partition by i.inventory_id order by rental_date) as "rented previously by",
  lead(c.first_name || ' ' || c.last_name) over (partition by i.inventory_id order by rental_date) as "rented later by"
FROM film
  inner join inventory i using (film_id)
  inner join rental r using(inventory_id)
  inner join customer c using(customer_id)
where title = 'BUCKET BROTHERHOOD';

-- same thing, but with a `window` clause to not repeat the same thing for every lag/lead

SELECT
  c.first_name || ' ' || c.last_name as "rented by",
  date_trunc('day', r.rental_date),
  lag(c.first_name || ' ' || c.last_name) over rd as "rented previously by",
  lead(c.first_name || ' ' || c.last_name) over rd as "rented later by"
FROM film
  inner join inventory i using (film_id)
  inner join rental r using(inventory_id)
  inner join customer c using(customer_id)
where title = 'BUCKET BROTHERHOOD'
window rd as (partition by i.inventory_id order by inventory_id, rental_date);