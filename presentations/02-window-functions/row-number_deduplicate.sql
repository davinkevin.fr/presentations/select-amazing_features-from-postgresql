create table film_with_duplicate (
    film_id integer default nextval('film_film_id_seq'::regclass) not null,
    title character varying(255) not null
);

-- Copy original list
insert into film_with_duplicate (title)
    -- all original films are inserted
    (select title from film)
union all
    -- and we insert 40 random from the same list
    (select title from film order by random() limit 40);

select count(*) as "number of films"
from film_with_duplicate;

-- lines with duplicated movies
select title, film_id
from film_with_duplicate
order by title;

-- lines with tag of iteration
select
  title,
  row_number() over (partition by title) as duplication_times,
  film_id
from film_with_duplicate;


-- Duplicates deletion
with iteration_of_each_film_entry as (
    select title, row_number() over (partition by title) as duplication_times, film_id
    from film_with_duplicate
)
delete from film_with_duplicate
where film_id IN (
    select distinct film_id
    from iteration_of_each_film_entry
    where duplication_times > 1
);

-- We have only 1000 movies after deletion
SELECT count(*) FROM film_with_duplicate;