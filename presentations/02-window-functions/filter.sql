-- ##########
-- # Rating #
-- ##########

select rating, count(*) as "number of movie"
from film
group by rating;

--

select
    count(*) as "all",
    count(*) filter ( where rating = 'PG' ) as "PG",
    count(*) filter ( where rating = 'G' ) as "G",
    count(*) filter ( where rating = 'NC-17' ) as "NC-17",
    count(*) filter ( where rating = 'PG-13' ) as "PG-13",
    count(*) filter ( where rating = 'R' ) as "R"
from film;

-- ##########
-- # Duration #
-- ##########

select
    count(*) filter ( where length > 30 ) as "> 30m",
    count(*) filter ( where length > 60 ) as "> 60m",
    count(*) filter ( where length > 90 ) as "> 90m",
    count(*) filter ( where length > 120 ) as "> 120m",
    count(*) filter ( where length > 180 ) as "> 180m"
from film;

-- ################
-- # Can be mixed #
-- ################

select
    count(*) as "all",
    count(*) filter ( where rating = 'PG' ) as "PG",
    count(*) filter ( where rating = 'G' ) as "G",
    count(*) filter ( where rating = 'NC-17' ) as "NC-17",
    count(*) filter ( where rating = 'PG-13' ) as "PG-13",
    count(*) filter ( where rating = 'R' ) as "R",
    count(*) filter ( where length > 30 ) as "> 30m",
    count(*) filter ( where length > 60 ) as "> 60m",
    count(*) filter ( where length > 90 ) as "> 90m",
    count(*) filter ( where length > 120 ) as "> 120m",
    count(*) filter ( where length > 180 ) as "> 180m"
from film;